<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpDocumentor' => array($vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'jtreminio\\TestExtensions' => array($vendorDir . '/jtreminio/test-extensions/src'),
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\Validator' => array($vendorDir . '/symfony/validator'),
    'Symfony\\Component\\Routing\\' => array($vendorDir . '/symfony/routing'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Ratchet' => array($vendorDir . '/cboden/ratchet/src'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'Guzzle\\Stream' => array($vendorDir . '/guzzle/stream'),
    'Guzzle\\Parser' => array($vendorDir . '/guzzle/parser'),
    'Guzzle\\Http' => array($vendorDir . '/guzzle/http'),
    'Guzzle\\Common' => array($vendorDir . '/guzzle/common'),
    'Evenement' => array($vendorDir . '/evenement/evenement/src'),
    'Doctrine\\DBAL' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common' => array($vendorDir . '/doctrine/common/lib'),
    'Client' => array($baseDir . '/src'),
    'Bramus' => array($vendorDir . '/bramus/router/src'),
    'Assetic' => array($vendorDir . '/kriswallsmith/assetic/src'),
    'App' => array($baseDir . '/src'),
);
